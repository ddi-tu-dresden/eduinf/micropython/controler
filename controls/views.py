from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json
import serial

def index(request):
    template = loader.get_template('controls/index.html')
    return HttpResponse(template.render())

@csrf_exempt 
def control(request):
    sendOverSerialBus(json.loads(request.body.decode("utf-8"))['pressedButton'])
    return HttpResponse(status = 200)

def sendOverSerialBus(data):
    print(data)
    ser = serial.Serial(port='COM1', baudrate=19200, bytesize=8, parity='N', stopbits=1, timeout=None, xonxoff=0, rtscts=0)
    if not ser.isOpen():
        ser.open()
    ser.write(data.encode())