# RETRO-CONTROL

A controller in retro style, prepared as a website. A web server is active in the background, which processes the input and forwards it to the serial bus.

## run the server

```python=
cd //actual path//
py manage.py runserver 80
```

## access

localhost:80/controls \
127.0.0.1:80/controls 

or 

http://localhost/controls \
http://127.0.0.1/controls

